import React from 'react'
import { Segment, Icon } from 'semantic-ui-react'
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const ChatHeader = ({ participants, messages, mostRecentDate }) => (
    <Segment className={styles.header}>
        <span className={styles.title}>
            <Icon name='rocketchat' /> Teletonn
        </span>
        <div className={styles.info}>
            <span>{participants} {' '} participants</span>
            <span>{messages} {' '} messages</span>
        </div>
        <span className={styles.date}>last message at {' '} {mostRecentDate}</span>
    </Segment>
)

ChatHeader.propTypes = {
    participants: PropTypes.number.isRequired,
    messages: PropTypes.number.isRequired,
    mostRecentDate: PropTypes.string.isRequired
};

export default ChatHeader;
