import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.css';

const MsgEditModale = ({ message, close, editFunc }) => {
    const [text, setText] = useState(message.text);

    const handleAddPost = async () => {
      if (!text) {
          return;
      }
      const date = Date.now();
      const updated = { ...message, text: text, editedAt: date};
      await editFunc(updated);
      close();
    };

    return (
        <Modal open onClose={close} size='tiny'>
            <Modal.Header>
                <span>Edit Comment</span>
            </Modal.Header>
            <Modal.Content>
                <Form onSubmit={handleAddPost} id='editForm'>
                    <Form.TextArea
                        name="text"
                        value={text}
                        placeholder="Enter message"
                        onChange={ev => setText(ev.target.value)}
                    />
                </Form>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    className={styles.btn}
                    icon='save outline'
                    floated="right"
                    form="editForm"
                    color="blue"
                    type="submit"
                    disabled={text === ''}
                    content='Save'
                />
                <Button className={styles.btn}floated="right" onClick={() => close()} basic>Cancel</Button>
            </Modal.Actions>
        </Modal>
    );
};

MsgEditModale.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
  editFunc: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default MsgEditModale;