import React from 'react'
import { Loader } from 'semantic-ui-react'
import Message from '../../components/Message';
import MsgEditModale from '../../components/MsgEditModale';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

class MassageBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editedMsg: undefined
        }
        let _prevDate = '';
        let _prevHeight = 0;
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.setEditMsg = this.setEditMsg.bind(this);
        this.isTheSameDate = this.isTheSameDate.bind(this);
        this.updatePrevDate = this.updatePrevDate.bind(this);
        this.getMassegeList = this.getMassegeList.bind(this);
    }

    scrollToBottom() {
        const scrollHeight = this.messageList.scrollHeight;
        const height = this.messageList.clientHeight;
        if (this._prevHeight !== scrollHeight) {
            this._prevHeight = scrollHeight;
            const maxScrollTop = scrollHeight - height;
            this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
        }
    }
      
    componentDidUpdate() {
        this.scrollToBottom();
    }

    setEditMsg(editMsg) {
        this.setState({ editedMsg: editMsg })
    }

    isTheSameDate(currentDate) {
        const formattedDate = this.props.timeConverter.getDateWithoutTime(currentDate);
        return this._prevDate === formattedDate;
    }

    updatePrevDate(newDate) {
        const formattedDate = this.props.timeConverter.getDateWithoutTime(newDate);
        this._prevDate = formattedDate;
    }

    getMassegeList(messages) {
        if (!messages || messages.length === 0) {
            return null;
        }

        const msgMap = (msg) => {
            const isNewDate = !this.isTheSameDate(msg.createdAt)
            const isCurrentUser = this.props.user.id === msg.userId;
            if (isNewDate) {
                this.updatePrevDate(msg.createdAt)
            }
            
            return <Message 
                key={msg.id}
                message={msg}
                timeConverter={this.props.timeConverter}
                isCurrentUser={isCurrentUser}
                isNewDate={isNewDate}
                updMassege={this.setEditMsg}
                deleteMessage={this.props.deleteMessage}
                likeMessage={this.props.likeMessage}
            />
        } 
        return messages.map(msg => msgMap(msg));
    };

    render() {
        return (
            <div className={styles.messages} ref={(div) => {this.messageList = div;}}>
                { this.props.isLoading 
                    ? <Loader content='Loading' active/>
                    : this.getMassegeList(this.props.messages)
                }
                <div ref={(el) => { this.messagesEnd = el; }}></div>
                {this.state.editedMsg &&
                    <MsgEditModale 
                        editFunc={this.props.updMassege} 
                        message={this.state.editedMsg} 
                        close={() => this.setEditMsg(undefined)}
                    />}
            </div>
        )
    }
};

MassageBox.propTypes = {
    isLoading: PropTypes.bool,
    messages: PropTypes.array.isRequired,
    timeConverter: PropTypes.objectOf(PropTypes.any).isRequired,
    user: PropTypes.objectOf(PropTypes.any).isRequired,
    deleteMessage: PropTypes.func.isRequired,
    updMassege: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired
};

export default MassageBox;
