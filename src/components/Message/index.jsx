import React, { useState } from 'react'
import { Icon, Divider, Popup, Confirm } from 'semantic-ui-react'
import { getUserImgLink } from '../../helpers/imageHelper'
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const Message = ({ message, timeConverter, isCurrentUser, isNewDate, deleteMessage, updMassege, likeMessage }) => {
    const [isDeleteConfirm, setDeleteConfirm] = useState(false);
    
    const divider = () => {
        return isNewDate 
                ? <Divider horizontal>{timeConverter.getDividerFormatDate(message.createdAt)}</Divider> 
                : null;
    }

    const getCurrentUserActions = () => {
        return (
            <div className={styles.myMsgActions}>
                <div className={`${styles.action} ${styles.myAction}`} onClick={() => setDeleteConfirm(true)} >
                    <Icon name='trash'/> <span>delete</span> 
                </div>
                <div className={`${styles.action} ${styles.myAction}`} onClick={() => updMassege(message)} >
                    <Icon name='edit'/> <span>edit</span> 
                </div>
            </div>
        );
    };

    const getLikeClasses = (isLiked) => {
        return isLiked ? `${styles.action} ${styles.liked} ${styles.like}` : `${styles.action} ${styles.like}`
    }

    return (
    <div>
        {divider()}
        <div className={isCurrentUser ? `${styles.messageBlock} ${styles.myMsgBlock}` : styles.messageBlock}> 
            <img className={styles.avatar} src={getUserImgLink(message.avatar)} alt='ava' /> 
            <div className={styles.message}>
                <div className={styles.header}>
                    <span className= {styles.userName}>{message.user}</span> 
                    <span className={styles.createdAt}>{`created at ${timeConverter.getHours(message.createdAt)}`}</span>
                </div>
                <p className= {styles.text}>
                    {message.text} 
                    {message.editedAt !== ""  ?
                        <Popup 
                            content={timeConverter.getEditFormatDate(message.editedAt)}
                            trigger={<span className={styles.edited}>(edited)</span>}
                            position='top center'
                        /> 
                        : null
                    }
                </p>
            </div>
            <div className={styles.actions}>
                {!isCurrentUser &&
                    <div className={getLikeClasses(message.isLiked)} onClick={() => likeMessage(message.id)}>
                        <Icon name='like'/> <span>like</span> 
                    </div>
                }
                {isCurrentUser ? getCurrentUserActions() : null}
            </div>
        </div>
        <Confirm
            size='mini'
            open={isDeleteConfirm}
            onCancel={() => setDeleteConfirm(false)}
            onConfirm={() => deleteMessage(message.id)}
        />
    </div>
    )

}

Message.propTypes = {
    message: PropTypes.objectOf(PropTypes.any).isRequired,
    timeConverter: PropTypes.objectOf(PropTypes.any).isRequired,
    isCurrentUser: PropTypes.bool.isRequired,
    isNewDate: PropTypes.bool.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    updMassege: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired
};

export default Message;
