import React, { useState } from 'react'
import { Segment, Form, TextArea, Button } from 'semantic-ui-react'
import uuid from 'uuid';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const InputBlock = ({ user, sendMessage }) => {
    const [text, setText] = useState('');

    const handleSend = () => {
        const messege = {
            id : uuid(),
            text,
            user : user.name,
            avatar : user.avatar,
            userId : user.id,
            editedAt : '',
            createdAt: Date.now()
        }
        sendMessage(messege);
        setText('');
    };

    return (
        <Segment clearing className={styles.inputBlock}>
            <Form> 
                <TextArea rows={2} placeholder='Tell us more' value={text} onChange={e => setText(e.target.value)} />
            </Form>
            <Button primary className={styles.btn} floated='right' onClick={handleSend}>Send</Button>
        </Segment>
    )
}

InputBlock.propTypes = {
    user: PropTypes.number.isRequired,
    sendMessage: PropTypes.func.isRequired
};

export default InputBlock;