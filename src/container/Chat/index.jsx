
import React from 'react';
import InputBlock from '../../components/InputBlock';
import ChatHeader from '../../components/Header';
import MessageBox from '../../components/MessageBox';
import { loadMessegesFake } from '../../services/messageService';
import { getCurrentUser } from '../../services/userService';
import * as timeConverter from '../../helpers/TimeConverter';


import styles from './styles.module.css';


class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: []
        }
        this.getParticipantCount = this.getParticipantCount.bind(this);
        this.addMessage = this.addMessage.bind(this);
        this.updateMessage = this.updateMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
        this.getLastMessageDate = this.getLastMessageDate.bind(this);
    }

    async componentDidMount() {
        this.setState({isLoading : true});
        const messeges = await loadMessegesFake();
        const user = await getCurrentUser();
        this.setState ({
            user,
            messages: messeges,
            isLoading : false
        })
    }
    
    addMessage(newMessage) {
        this.setState({
            ...this.state,
            messages: newMessage = [...this.state.messages, newMessage]
        });
    }

    updateMessage(updatedMessage) {
        const updMasseges = this.state.messages.map(msg => (msg.id === updatedMessage.id ? updatedMessage : msg));
        this.setState({
            ...this.state,
            messages: updMasseges
        });
    }

    deleteMessage(MsgId) {
        const updMasseges = this.state.messages.filter(msg => (msg.id !== MsgId));
        this.setState({...this.state, messages: updMasseges });
    }
    
    getParticipantCount () {
        var unicUsers = new Set();
        this.state.messages.map(msg => unicUsers.add(msg.user));
        return unicUsers.size;
    }

    likeMessage(messageId) {
        const updMessage = (msg) => {
            return msg.isLiked ? { ...msg, isLiked: false } : { ...msg, isLiked: true };
        }
        const updMasseges = this.state.messages.map(msg => (msg.id === messageId ? updMessage(msg) : msg));
        this.setState({ ...this.state, messages: updMasseges });
    }

    getLastMessageDate() {
        const messages = this.state.messages;
        if (messages.length === 0) {
            return 'never';
        }
        const lastMsg = this.state.messages[messages.length -1];
        return timeConverter.getDividerFormatDate(lastMsg.createdAt);
    }

    render() {
        return (
            <div className={styles.application}>
                <ChatHeader
                    participants={this.getParticipantCount()}
                    messages={this.state.messages.length}
                    mostRecentDate={this.getLastMessageDate()}
                />
                <MessageBox
                    isLoading={this.state.isLoading}
                    messages={this.state.messages}
                    timeConverter={timeConverter}
                    updMassege={this.updateMessage}
                    deleteMessage={this.deleteMessage}
                    user={this.state.user}
                    likeMessage={this.likeMessage}
                />
                <InputBlock user={this.state.user} sendMessage={this.addMessage}/>
            </div>
        );
    }

}

export default Chat;