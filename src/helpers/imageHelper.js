export const getUserImgLink = imageUrl => {
  if (imageUrl && imageUrl !== '') {
    return imageUrl;
  } else {
    return 'https://miro.medium.com/max/720/1*W35QUSvGpcLuxPo3SRTH4w.png';
  }
}
