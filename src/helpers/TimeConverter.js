const MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May', 
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

const DAYS = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday', 
    'Friday',
    'Saturday'
];

const getSuffix = (day) => {
    switch(day) {
        case 1:  
            return 'st';
        case 2:  
            return 'nd';
        case 3:  
            return 'rd';
        default:
            return 'th';
      }    
}

export const getDividerFormatDate = (date) => {
    const msgDate = new Date(date);
    const currentDay = new Date();
    const parseDate = (d) => {
        return `${MONTHS[d.getMonth()]}, ${DAYS[d.getDay()]}, ${d.getDate()}${getSuffix(d.getDate())}`
    } 
    const parsedDate = parseDate(msgDate);
    
    if (parsedDate === parseDate(currentDay)) {
        return 'Today';
    }

    currentDay.setDate(currentDay.getDate() - 1);
    if (parsedDate === parseDate(currentDay)) {
        return 'Yesterday';
    }
    
    return parsedDate;
}

export const getDateWithoutTime = (date) => {
    const parsedDate = new Date(date);
    return `${parsedDate.getDay()}-${parsedDate.getMonth()}-${parsedDate.getFullYear()}`;
}

export const getHours = (date) => {
    const parsedDate = new Date(date);
    return `${parsedDate.getHours()}:${parsedDate.getMinutes()}`;
}

export const getEditFormatDate = (date) => {
    const firstPart = getDividerFormatDate(date);
    const secondPart = getHours(date);
    return `${firstPart} at ${secondPart}`;
}